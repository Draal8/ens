void verrouiller (int *verrou) {
  while (*verrou != 0)
    ;                    // attente active
  *verrou = 1 ;
}
void deverrouiller (int *verrou) {
  *verrou = 0 ;
}
