verrouiller:			// argument dans la pile
	ld   [%sp+1],%a		// a $\leftarrow$ adresse du verrou
	push %b			// on va utiliser b, le sauvegarder d'abord
boucle:
	fas  [%a],%b		// b $\leftarrow$ *verrou, puis *verrou $\leftarrow$ 1
        cmp  0,%b		// teste la valeur de b
	jne  boucle		// boucle si b $\neq$ 0
	pop  %b			// restaurer b
        rtn
