#
# Cibles possibles :
# all: tous les chapitres individuels (chx-yyy.pdf)
# chx-yyy.pdf: un chapitre individuel particulier
# ...
# tout.pdf: document contenant tous les chapitres
# print: tous les PDF au format "6 pages par page"
#

.SUFFIXES:	.pdf .dot .fig .svg .gnu .tex

.fig.pdf:
	fig2dev -L pdf $*.fig $*.pdf

.dot.pdf:
	dot -T pdf $*.dot > $*.pdf

.svg.pdf:
	inkscape --export-pdf=$*.pdf $*.svg

.gnu.pdf:
	gnuplot < $*.gnu > $*.pdf

.tex.pdf:
	pdflatex $*
	pdflatex $*

# pour la cible print
PRINTCMD = pdfjam --quiet --paper a4paper --keepinfo \
	--nup 2x3 --frame true --delta "0.2cm 0.2cm" --scale 0.95

DEPS	= courspda.sty casserole.pdf term.pdf penseur.pdf logo-uds.pdf \
	annee.tex \
	licence.tex by-nc.pdf

#
# Extraction (optionnelle) des caracteristiques des processeurs de la
# base des CPU
#
CPUDBDIR = ../cpudb
GENCPU = ./cpudb-extract

##############################################################################
# Introduction

SRCintro = ch1-intro.tex sl1-intro.tex

FIGintro = \
	inc1-intro/arch-linux.pdf \
	inc1-intro/arch-w2k.pdf \
	inc1-intro/arch-android.pdf \
	inc1-intro/arch-ios.pdf \
	inc1-intro/noyau.pdf \
	inc1-intro/acces.pdf \

IMGintro = \

LSTintro = \

##############################################################################
# Mécanismes de base

SRCbase = ch2-base.tex sl2-base.tex

FIGbase = \
	inc2-base/zorglub33.pdf \
	inc2-base/appel-pile.pdf \
	inc2-base/z33-irq.pdf \
	inc2-base/commut.pdf \
	inc2-base/reg.pdf \
	inc2-base/zorglub33m-1.pdf \
	inc2-base/zorglub33m-2.pdf \
	inc2-base/ps-noyau.pdf \
	inc2-base/chgt-pile.pdf \
	inc2-base/esp-adr.pdf \
	inc2-base/acces.pdf \

IMGbase = \

LSTbase = \
	inc2-base/kbd-read.s \
	inc2-base/kbd-led.s \
	inc2-base/irq.s \
	inc2-base/struct-proc.c \
	inc2-base/lasuite.c \
	inc2-base/appel.c \
	inc2-base/appel.s \
	inc2-base/open.c \
	inc2-base/open.s \
	inc2-base/trap.c \
	inc2-base/sysopen.c \
	inc2-base/sysread.c \
	inc2-base/swtch.s \
	inc2-base/pb-mem.c \
	inc2-base/exo-cc.c \
	inc2-base/exo-cc.s \
	inc2-base/nm.c \
	inc2-base/nm.sh \

##############################################################################
# Processus et threads

SRCpsthr = ch3-psthr.tex sl3-psthr.tex

FIGpsthr = \
	inc3-psthr/etats.pdf \
	inc3-psthr/var.pdf \
	inc3-psthr/psmem-1.pdf \
	inc3-psthr/psmem-2.pdf \
	inc3-psthr/psmem-3.pdf \
	inc3-psthr/crit.pdf \
	inc3-psthr/decay.pdf \
	inc3-psthr/thr-intro.pdf \
	inc3-psthr/thr-ultklt.pdf \
	inc3-psthr/thr-stack.pdf \
	inc3-psthr/thr-api.pdf \
	inc3-psthr/thr-sig.pdf \
	inc3-psthr/bar-mouton.pdf \
	inc3-psthr/bar-velo.pdf \
	inc3-psthr/bar-intro.pdf \
	inc3-psthr/bar-prtg.pdf \


IMGpsthr = \

LSTpsthr = \
	inc3-psthr/ps.sh \
	inc3-psthr/it-horl.c \
	inc3-psthr/exo-stack.c \
	inc3-psthr/thr.c \
	inc3-psthr/thr-narg.c \
	inc3-psthr/strtok.c \
	inc3-psthr/ctime.c \
	inc3-psthr/errno.c \
	inc3-psthr/thr-attr.c \
	inc3-psthr/bar-attr.c \

###############################################################################
# Concurrence & synchronisation

SRCsynch = ch4-synch.tex sl4-synch.tex

FIGsynch = \
	inc4-synch/motiv-collab.pdf \
	inc4-synch/motiv-inact.pdf \
	inc4-synch/motiv-part.pdf \
	inc4-synch/motiv-scal.pdf \
	inc4-synch/cpu-freq.pdf \
	inc4-synch/cpu-transist.pdf \
	inc4-synch/cpu-gravure.pdf \
	inc4-synch/arbre-exec.pdf \
	inc4-synch/zorglub33p.pdf \
	inc4-synch/sect-crit.pdf \
	inc4-synch/sig-mask.pdf \
	inc4-synch/parking.pdf \
	inc4-synch/prodcons-infini.pdf \
	inc4-synch/prodcons-borne.pdf \
	inc4-synch/gralloc.pdf \
	inc4-synch/gr-0.pdf \
	inc4-synch/gr-1.pdf \
	inc4-synch/gr-sur.pdf \
	inc4-synch/arbexec-cond1.pdf \
	inc4-synch/arbexec-cond2.pdf \
	inc4-synch/arbexec-cond3.pdf \

IMGsynch = \
	inc4-synch/sem-creach.jpg \
	inc4-synch/sem-sncf-ferme.jpg \
	inc4-synch/sem-sncf-ouvert.jpg \

LSTsynch = \
	inc4-synch/varcrit.c \
	inc4-synch/varcrit.s \
	inc4-synch/sig.c \
	inc4-synch/file.c \
	inc4-synch/lock-naif.c \
	inc4-synch/lock-naif.s \
	inc4-synch/lock-peterson.c \
	inc4-synch/algo-fas.c \
	inc4-synch/lock-fas.c \
	inc4-synch/lock-fas.s \
	inc4-synch/lock-open.c \
	inc4-synch/lock-lockf.c \
	inc4-synch/mutex-init.c \
	inc4-synch/lock-sig.c \
	inc4-synch/reflexe1.c \
	inc4-synch/reflexe2.c \
	inc4-synch/struct-partage.c \
	inc4-synch/sem-act.c \
	inc4-synch/sem-spin.c \
	inc4-synch/sem-passif.c \
	inc4-synch/lect-ecr.c \
	inc4-synch/prodcons-borne.c \
	inc4-synch/prodcons-infini.c \
	inc4-synch/cond1.c \
	inc4-synch/cond2.c \
	inc4-synch/cond3.c \
	inc4-synch/cond4.c \
	inc4-synch/cond-posix.c \
	inc4-synch/exo-cond.c \
	inc4-synch/sleep-fix.c \
	inc4-synch/kern-int.c \
	inc4-synch/kern-int.s \
	inc4-synch/kern-proc.c \
	inc4-synch/kern-sleep.c \
	\

##############################################################################
# Mémoire

SRCmem = ch5-mem.tex sl5-mem.tex

FIGmem = \
	inc5-mem/psmem-3.pdf \
	inc5-mem/esp-adr.pdf \
	inc5-mem/zorglub33m-1.pdf \
	inc5-mem/mmu-z33m.pdf \
	inc5-mem/psmem-1.pdf \
	inc5-mem/mmu-ipc-1.pdf \
	inc5-mem/mmu-ipc-2.pdf \
	inc5-mem/zorglub33s.pdf \
	inc5-mem/mmu-pdp11a.pdf \
	inc5-mem/mmu-pdp11b-1.pdf \
	inc5-mem/mmu-pdp11b-2.pdf \
	inc5-mem/mmu-pdp11ex.pdf \
	inc5-mem/shlib.pdf \
	inc5-mem/swap.pdf \
	inc5-mem/zorglub33v.pdf \
	inc5-mem/tablen.pdf \
	inc5-mem/table3.pdf \
	inc5-mem/tlb.pdf \
	inc5-mem/kern-page.pdf \
	inc5-mem/mmu-i386a.pdf \
	inc5-mem/mmu-i386b.pdf \
	inc5-mem/mmu-pte386-1.pdf \
	inc5-mem/mmu-pte386-2.pdf \
	inc5-mem/mmu-tlb386.pdf \
	inc5-mem/mmu-x64.pdf \
	inc5-mem/page.pdf \
	inc5-mem/regions.pdf \
	inc5-mem/mmap-file.pdf \
	inc5-mem/mmap-shmem.pdf \
	inc5-mem/shmat.pdf \

IMGmem = \
	inc5-mem/hp-ipc.jpg \
	inc5-mem/pdp11.jpg \

LSTmem = \
	inc5-mem/swap.sh \
	inc5-mem/regions.sh \
	inc5-mem/vm-ps.sh \
	inc5-mem/mmap-ex1.c \
	inc5-mem/mmap-ex2-ftrunc.c \
	inc5-mem/mmap-ex3-shmopen.c \
	inc5-mem/mmap-struct.c \
	inc5-mem/mmap-sync.c \

##############################################################################
# Périphériques

SRCdev = ch6-dev.tex sl6-dev.tex

FIGdev = \
	inc6-dev/arch-now.pdf \
	inc6-dev/zorglub33.pdf \
	inc6-dev/reg.pdf \
	inc6-dev/kbd-ctrl.pdf \
	inc6-dev/dynam.pdf \
	inc6-dev/pci.pdf \
	inc6-dev/conf-pci.pdf \
	inc6-dev/cdevsw.pdf \
	inc6-dev/pty.pdf \
	inc6-dev/spec.pdf \
	inc6-dev/impr.pdf \

IMGdev = \
	inc6-dev/pdp11.jpg \
	inc6-dev/term-vt100.jpg \

LSTdev = \
	inc6-dev/kbd-read.s \
	inc6-dev/kbd-led.s \
	inc6-dev/dsk.c \
	inc6-dev/lpr.c \
	inc6-dev/smart.sh \
	inc6-dev/lsdev.sh \

##############################################################################
# Système de fichiers

SRCfs = ch7-fs.tex sl7-fs.tex

FIGfs = \
	inc7-fs/pile-0.pdf \
	inc7-fs/pile-1.pdf \
	inc7-fs/pile-2.pdf \
	inc7-fs/pile-3.pdf \
	inc7-fs/pile-4.pdf \
	inc7-fs/pile-5.pdf \
	inc7-fs/disque.pdf \
	inc7-fs/bsdpart.pdf \
	inc7-fs/dospart.pdf \
	inc7-fs/volume.pdf \
	inc7-fs/geometrie.pdf \
	inc7-fs/buffer.pdf \
	inc7-fs/bcache.pdf \
	inc7-fs/arbo.pdf \
	inc7-fs/lien-4.pdf \
	inc7-fs/freelist.pdf \
	inc7-fs/inode.pdf \
	inc7-fs/mvtoto.pdf \
	inc7-fs/log.pdf \
	inc7-fs/mount-0.pdf \
	inc7-fs/mount-1.pdf \
	inc7-fs/mount-2.pdf \
	inc7-fs/vfs.pdf \

IMGfs = \

LSTfs = \
	inc7-fs/vmstat.sh \
	inc7-fs/ls-i.sh \
	inc7-fs/mvtoto.c \

##############################################################################
# Machines virtuelles

SRCvirt = ch8-virt.tex sl8-virt.tex

FIGvirt = \
	inc8-virt/taxo.pdf \
	inc8-virt/type0.pdf \
	inc8-virt/type1.pdf \
	inc8-virt/type15.pdf \
	inc8-virt/type2.pdf \
	inc8-virt/cont.pdf \
	inc8-virt/kern-vm.pdf \
	inc8-virt/trap-emul.pdf \
	inc8-virt/ring.pdf \
	inc8-virt/vmx.pdf \
	inc8-virt/vmem.pdf \
	inc8-virt/ept.pdf \

IMGvirt = \
	inc8-virt/screenshot.jpg \

LSTvirt = \

##############################################################################
# Structures des SE

SRCstruct = ch9-struct.tex sl9-struct.tex

FIGstruct = \
	inc9-struct/arch-linux.pdf \
	inc9-struct/acces.pdf \
	inc9-struct/microk.pdf \

IMGstruct = \
	inc9-struct/franquin-zorglub.jpg \

LSTstruct = \

##############################################################################
# L'ensemble

SRCall = \
	$(SRCintro) \
	$(SRCbase) \
	$(SRCpsthr) \
	$(SRCsynch) \
	$(SRCmem) \
	$(SRCdev) \
	$(SRCfs) \
	$(SRCvirt) \
	$(SRCstruct) \
	tout.tex

FIGall = \
	$(FIGintro) \
	$(FIGbase) \
	$(FIGpsthr) \
	$(FIGsynch) \
	$(FIGmem) \
	$(FIGdev) \
	$(FIGfs) \
	$(FIGvirt) \
	$(FIGstruct) \

IMGall = \
	$(IMGintro) \
	$(IMGbase) \
	$(IMGpsthr) \
	$(IMGmem) \
	$(IMGsynch) \
	$(IMGdev) \
	$(IMGvirt) \
	$(IMGstruct) \

LSTall = \
	$(LSTintro) \
	$(LSTbase) \
	$(LSTpsthr) \
	$(LSTsynch) \
	$(LSTmem) \
	$(LSTdev) \
	$(LSTfs) \
	$(LSTvirt) \
	$(LSTstruct) \

##############################################################################
# Les cibles
##############################################################################

all:	ch1-intro.pdf \
	ch2-base.pdf \
	ch3-psthr.pdf \
	ch4-synch.pdf \
	ch5-mem.pdf \
	ch6-dev.pdf \
	ch7-fs.pdf \
	ch8-virt.pdf \
	ch9-struct.pdf \

ch1-intro.pdf:	$(DEPS) $(FIGintro) $(IMGintro) $(LSTintro) $(SRCintro)
ch2-base.pdf:	$(DEPS) $(FIGbase) $(IMGbase) $(LSTbase) $(SRCbase)
ch3-psthr.pdf:	$(DEPS) $(FIGpsthr) $(IMGpsthr) $(LSTpsthr) $(SRCpsthr)
ch4-synch.pdf:	$(DEPS) $(FIGsynch) $(IMGsynch) $(LSTsynch) $(SRCsynch)
ch5-mem.pdf:	$(DEPS) $(FIGmem) $(IMGmem) $(LSTmem) $(SRCmem)
ch6-dev.pdf:	$(DEPS) $(FIGdev) $(IMGdev) $(LSTdev) $(SRCdev)
ch7-fs.pdf:	$(DEPS) $(FIGfs) $(IMGfs) $(LSTfs) $(SRCfs)
ch8-virt.pdf:	$(DEPS) $(FIGvirt) $(IMGvirt) $(LSTvirt) $(SRCvirt)
ch9-struct.pdf:	$(DEPS) $(FIGstruct) $(IMGstruct) $(LSTstruct) $(SRCstruct)

inc2-base/zorglub33m-1.pdf: inc2-base/zorglub33m.fig
	./figlayers 50-99       < $< | fig2dev -L pdf /dev/stdin $@
inc2-base/zorglub33m-2.pdf: inc2-base/zorglub33m.fig
	./figlayers 40-99       < $< | fig2dev -L pdf /dev/stdin $@

inc3-psthr/psmem-1.pdf: inc3-psthr/psmem.fig
	./figlayers 30-39       < $< | fig2dev -L pdf /dev/stdin $@
inc3-psthr/psmem-2.pdf: inc3-psthr/psmem.fig
	./figlayers 50-69       < $< | fig2dev -L pdf /dev/stdin $@
inc3-psthr/psmem-3.pdf: inc3-psthr/psmem.fig
	./figlayers 30-99       < $< | fig2dev -L pdf /dev/stdin $@

# reproduits ici pour simplicité, identiques à inc2-base/
inc5-mem/zorglub33m-1.pdf: inc2-base/zorglub33m.fig
	./figlayers 50-99       < $< | fig2dev -L pdf /dev/stdin $@

# reproduits ici pour simplicité, identiques à inc3-psthr/
inc5-mem/psmem-1.pdf: inc3-psthr/psmem.fig
	./figlayers 30-39       < $< | fig2dev -L pdf /dev/stdin $@
inc5-mem/psmem-3.pdf: inc3-psthr/psmem.fig
	./figlayers 30-99       < $< | fig2dev -L pdf /dev/stdin $@

inc5-mem/mmu-ipc-1.pdf: inc5-mem/mmu-ipc.fig
	./figlayers 50-99       < $< | fig2dev -L pdf /dev/stdin $@
inc5-mem/mmu-ipc-2.pdf: inc5-mem/mmu-ipc.fig
	./figlayers 40-99       < $< | fig2dev -L pdf /dev/stdin $@

inc5-mem/mmu-pdp11b-1.pdf: inc5-mem/mmu-pdp11b.fig
	./figlayers 40 50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc5-mem/mmu-pdp11b-2.pdf: inc5-mem/mmu-pdp11b.fig
	./figlayers 30 50-99    < $< | fig2dev -L pdf /dev/stdin $@

inc5-mem/mmu-pte386-1.pdf: inc5-mem/mmu-pte386.fig
	./figlayers 40 50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc5-mem/mmu-pte386-2.pdf: inc5-mem/mmu-pte386.fig
	./figlayers 30 50-99    < $< | fig2dev -L pdf /dev/stdin $@

inc7-fs/pile-0.pdf: inc7-fs/pile.fig
	./figlayers       50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc7-fs/pile-1.pdf: inc7-fs/pile.fig
	./figlayers 41    50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc7-fs/pile-2.pdf: inc7-fs/pile.fig
	./figlayers 42    50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc7-fs/pile-3.pdf: inc7-fs/pile.fig
	./figlayers 43    50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc7-fs/pile-4.pdf: inc7-fs/pile.fig
	./figlayers 44    50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc7-fs/pile-5.pdf: inc7-fs/pile.fig
	./figlayers 45    50-99    < $< | fig2dev -L pdf /dev/stdin $@

inc7-fs/lien-4.pdf: inc7-fs/lien.fig
	./figlayers 30-60          < $< | fig2dev -L pdf /dev/stdin $@

inc7-fs/mount-0.pdf: inc7-fs/mount.fig
	./figlayers       50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc7-fs/mount-1.pdf: inc7-fs/mount.fig
	./figlayers 40    50-99    < $< | fig2dev -L pdf /dev/stdin $@
inc7-fs/mount-2.pdf: inc7-fs/mount.fig
	./figlayers 40-49 50-99    < $< | fig2dev -L pdf /dev/stdin $@

tout.pdf:	$(DEPS) $(FIGall) $(LSTall) $(SRCall)

print:	print-tout.pdf

print-tout.pdf: tout.pdf
	$(PRINTCMD) -o print-tout.pdf tout.pdf

printtps:	all tps.pdf
	$(PRINTCMD) -o print-tps.pdf tps.pdf

tps.tex: tout.tex
	sed \
		-e 's/Architecture des //' \
		-e '/sl[6-9]-/d' \
		-e 's/, *fichier.*/}/' \
		$< > $@

gencpu:
	@test -d $(CPUDBDIR)  -a -f $(CPUDBDIR)/processor.csv \
		|| (echo "Telecharger la DB depuis http://cpudb.stanford.edu/" dans $(CPUDBDIR) ; exit 1)
	@test "x$$PGUSER" != x -a "x$$PGPASSWORD" != x \
		|| (echo "Il faut initialiser PGUSER et PGPASSWORD" ; exit 1)
	$(GENCPU) $(CPUDBDIR) inc1-intro/cpu-


clean:
	rm -f $(FIGall)
	rm -f *.bak */*.bak *.nav *.out *.snm *.vrb *.log *.toc *.aux
	rm -f print-*.pdf ch*.pdf tout*.pdf by-nc.pdf casserole.pdf
	rm -f tps*
	rm -f inc?-?-*/a.out
