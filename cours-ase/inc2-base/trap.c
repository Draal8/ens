int (*tabprim [NPRIM]) (void) = {	// tableau de pointeurs sur fonctions renvoyant un entier
    [1] = sys_fork,			// primitive n° 1 : fork
    [2] = sys_exit,			// primitive n° 2 : exit
    ...
    [5] = sys_open,			// primitive n° 5 : open
    ...
} ;
struct proc *curproc ;			// descripteur du processus courant (variable à l'adr 1000)

void traiter_trap (void)		// appelée par le code traitant des interruptions et exceptions
{
    int numero, retour ;

    numero = curproc->a ;		// numéro de la primitive $\leftarrow$ valeur du registre A sauvegardée
    retour = (*tabprim [numero]) () ;	// appeler la primitive
    curproc->a = retour ;		// valeur de A sauvegardée $\leftarrow$ retour de la primitive
}
