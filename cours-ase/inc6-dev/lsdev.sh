> cat /proc/devices
Character devices:
...
239 nvme
...
Block devices:
...
259 blkext
...

> ls /dev/nvme*
crw------- 1 root root 239, 0 Jul  2 08:28 /dev/nvme0
brw-rw---- 1 root disk 259, 0 Jul  2 08:28 /dev/nvme0n1
brw-rw---- 1 root disk 259, 1 Jul  2 08:28 /dev/nvme0n1p1
brw-rw---- 1 root disk 259, 2 Jul  2 08:28 /dev/nvme0n1p2
