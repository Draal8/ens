> sudo smartctl --scan
/dev/nvme0 -d nvme # /dev/nvme0, NVMe device

> sudo smartctl --all /dev/nvme0
...
Temperature:                        45 Celsius
Available Spare:                    100%
Available Spare Threshold:          50%
Percentage Used:                    1%
Data Units Read:                    8 577 655 [4.39 TB]
Data Units Written:                 15 659 771 [8.01 TB]
...
Media and Data Integrity Errors:    0
Error Information Log Entries:      0
...
